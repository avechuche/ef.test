﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using EF.Test.Models;

namespace EF.Test.Repositories
{
    public class OrderRepository<TContext, TEntity> where TContext : DbContext, new() where TEntity : Order
    {
        public void Add(TEntity currentOrder)
        {
            using (var customContext = new TContext())
            {
                customContext.Set<TEntity>().Add(currentOrder);
                customContext.SaveChanges();
            }
        }

        public void Update(TEntity currentEntity, object newValues)
        {
            using (var customContext = new TContext())
            {
                customContext.Set<TEntity>().Attach(currentEntity);
                customContext.Entry(currentEntity).CurrentValues.SetValues(newValues);
                foreach (var currentEntityInfo in currentEntity.GetType().GetProperties())
                {
                    foreach (var updateEntityInfo in newValues.GetType().GetProperties())
                    {
                        if (currentEntityInfo.Name == updateEntityInfo.Name)
                        {
                            if (IsCollection(new[] { currentEntityInfo, updateEntityInfo }))
                            {
                                foreach (var currentItem in (IEnumerable)updateEntityInfo.GetValue(newValues))
                                {
                                    currentEntityInfo.PropertyType.GetMethod("Add")
                                        ?.Invoke(currentEntityInfo.GetValue(currentEntity), new[] { currentItem });
                                }
                            }
                            //https://stackoverflow.com/questions/23793845/how-do-i-determine-if-a-property-is-a-user-defined-type-in-c
                        }
                    }
                }
                customContext.SaveChanges();
            }
        }

        private static bool IsCollection(IEnumerable<PropertyInfo> propertiesInfo)
        {
            return propertiesInfo.All(x => x.PropertyType.IsGenericType &&
                                          typeof(IEnumerable).IsAssignableFrom(x.PropertyType
                                              .GetGenericTypeDefinition()) ||
                                          x.PropertyType.GetInterfaces().Any(currentInterface =>
                                              currentInterface.IsGenericType &&
                                              currentInterface.GetGenericTypeDefinition() == typeof(IEnumerable)));
        }

        public bool TryGetValue(Expression<Func<TEntity, bool>> currentExpression, out TEntity value)
        {
            using (var customContext = new TContext())
            {
                var currentEntity = customContext.Set<TEntity>().FirstOrDefault(currentExpression);
                if (currentEntity != null)
                {
                    value = currentEntity;
                    return true;
                }
                value = default;
                return false;
            }
        }

    }
}
using EF.Test.Context;
using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using EF.Test.Models;
using EF.Test.Repositories;
using System.Linq;

namespace EF.Test.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly OrderRepository<CustomContext, Order> _orderRepository = new OrderRepository<CustomContext, Order>();

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            WorkAll();
            WorkOrNotWork();
        }

        public void WorkAll()
        {
            try
            {
                using (var dbContext = new CustomContext())
                {
                    dbContext.Orders.Add(new Order
                    {
                        Price = 1,
                        Quantity = 1,
                        OtherID = "1",
                        Option = new Option
                        {
                            OneProperty = "1",
                            TwoProperty = "1",
                            OtherProperty = 1
                        },
                        CreatedOn = DateTimeOffset.UtcNow,
                    });

                    dbContext.Orders.Add(new Order
                    {
                        Price = 2,
                        Quantity = 2,
                        OtherID = "2",
                        Option = new Option
                        {
                            OneProperty = "2",
                            TwoProperty = "2",
                            OtherProperty = 2
                        },
                        CreatedOn = DateTimeOffset.UtcNow,
                    });

                    dbContext.SaveChanges();

                    var currentOrder = dbContext.Orders.FirstOrDefault(x => x.OtherID == "1");

                    dbContext.Orders.Add(new Order
                    {
                        Price = 3,
                        Quantity = 3,
                        OtherID = "3",
                        Option = new Option
                        {
                            OneProperty = "3",
                            TwoProperty = "3",
                            OtherProperty = 3
                        },
                        CreatedOn = DateTimeOffset.UtcNow,
                        Orders = new List<Order> { currentOrder }
                    });

                    currentOrder = dbContext.Orders.FirstOrDefault(x => x.OtherID == "2");

                    currentOrder.Orders.Add(new Order
                    {
                        Price = 4,
                        Quantity = 4,
                        OtherID = "4",
                        Option = new Option
                        {
                            OneProperty = "4",
                            TwoProperty = "4",
                            OtherProperty = 4
                        },
                        CreatedOn = DateTimeOffset.UtcNow
                    });

                    dbContext.SaveChanges();

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public void WorkOrNotWork()
        {
            // WORK
            _orderRepository.Add(new Order
            {
                Price = 1,
                Quantity = 1,
                OtherID = "1",
                Option = new Option
                {
                    OneProperty = "1",
                    TwoProperty = "1",
                    OtherProperty = 1
                },
                CreatedOn = DateTimeOffset.UtcNow,
            });

            // WORK
            _orderRepository.Add(new Order
            {
                Price = 2,
                Quantity = 2,
                OtherID = "2",
                Option = new Option
                {
                    OneProperty = "2",
                    TwoProperty = "2",
                    OtherProperty = 2
                },
                CreatedOn = DateTimeOffset.UtcNow
            });

            //NOT WORK. Cannot add "localItem" to "new Order" because (localItem) already exist in DB. I need to add a reference, not add item to db again
            if (_orderRepository.TryGetValue(x => x.OtherID == "1", out var localItem))
            {
                _orderRepository.Add(new Order
                {
                    Price = 3,
                    Quantity = 3,
                    OtherID = "3",
                    Option = new Option
                    {
                        OneProperty = "3",
                        TwoProperty = "3",
                        OtherProperty = 3
                    },
                    CreatedOn = DateTimeOffset.UtcNow,
                    Orders = new List<Order> { localItem }
                });
            }

            // NOT WORK. Cannot update "orderOne" because 'orderTwo' already exist in DB. I need to add a reference, not add item to db again
            // _orderRepository.Update It works correctly when the item in the list does NOT exist in the database yet
            if (_orderRepository.TryGetValue(x => x.OtherID == "1", out var orderOne))
            {
                if (_orderRepository.TryGetValue(x => x.OtherID == "2", out var orderTwo))
                {
                    _orderRepository.Update(orderOne, new
                    {
                        Orders = new List<Order>
                        {
                            orderTwo
                        }
                    });
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EF.Test.Models
{
    public class Order
    {
        public int OrderID { get; set; }

        [StringLength(450)]
        [Index(IsUnique = true)]
        public string OtherID { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public DateTimeOffset CreatedOn { get; set; }

        public int OptionID { get; set; }

        public Option Option { get; set; }

        public ICollection<Order> Orders { get; set; } = new List<Order>(); //1-Many Relation
    }
}

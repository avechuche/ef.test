﻿namespace EF.Test.Models
{
    public class Option
    {
        public int OptionID { get; set; }

        public string OneProperty { get; set; }

        public string TwoProperty { get; set; }

        public int OtherProperty { get; set; }
    }
}

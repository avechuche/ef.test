﻿using System.Data.Entity;
using EF.Test.Models;

namespace EF.Test.Context
{
    public class CustomContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }

        public DbSet<Option> Options { get; set; }

        #region Constructors

        public CustomContext() : base("name=Context.Front")
        {
            Database.SetInitializer(new BotContextInitializer());
        }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Order>().ToTable(nameof(Order));
            modelBuilder.Entity<Option>().ToTable(nameof(Option));

            //modelBuilder.Entity<Order>()
            //    .HasOptional(x => x.Option)
            //    .WithRequired(x => x.Order);

            modelBuilder.Entity<Order>()
                .HasMany(x => x.Orders)
                .WithMany()
                .Map(m =>
                {
                    m.MapLeftKey("SellOrderID");
                    m.MapRightKey("BuyOrderID");
                    m.ToTable("SellBuyOrders");
                });
        }

        private class BotContextInitializer : DropCreateDatabaseAlways<CustomContext> { }
    }

}


